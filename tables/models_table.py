from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import datetime

Base = declarative_base()

# Association de la table Product & Catégory
product_category = Table(
    "product_category",
    Base.metadata,
    Column("id_product", Integer, ForeignKey("product.id")),
    Column("id_category", Integer, ForeignKey("category.id")),
)


class Product(Base):
    """
    class model for product table DB
    """

    __tablename__ = "product"

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    stores = Column(String(255))
    url = Column(String(255))
    nutriscore = Column(Integer)

    categories = relationship(
        "Category", secondary=product_category, back_populates="products"
    )


class Category(Base):
    """
    class model for category table DB
    """

    __tablename__ = "category"

    id = Column(Integer, primary_key=True)
    name = Column(String(255))

    products = relationship(
        "Product", secondary=product_category, back_populates="categories"
    )


class History(Base):
    """
    class model for history table DB
    """

    __tablename__ = "history"

    id = Column(Integer, primary_key=True)
    id_user = Column(Integer)  # A mettre égal à 1
    date_create = Column(DateTime, default=datetime.datetime.utcnow)
    id_aliment = Column(Integer, ForeignKey("product.id"))
    id_substitut = Column(Integer, ForeignKey("product.id"))
