from tables.recover_api import RecoverApi
from tables.query import Query
from tables.config.config import config


class Manage:
    """
    class manage for run script
    """

    def __init__(self):
        """
        Constructor for Substitut
        """
        self.create_table = config["MANAGE"].getboolean("Create_table")
        self.drop_all_table = config["MANAGE"].getboolean("Drop_all_table")
        self.delete_tables = config["MANAGE"].getboolean("delete_data_tables")
        self.recover_api_product = config["MANAGE"].getboolean("Insert_product")

        self.limit_product = config["CATEGORY"]["max_product_by_category"]
        self.list_categories = config["CATEGORY"]["list_categories"]
        self.query = Query()

        self.manage()

    def manage(self):
        """
        recovery of the config file
        & launch of the functions which are in "true"
        """
        if self.drop_all_table:
            print("Drop table")
            self.query.drop_all_table()

        if self.create_table:
            print("Create table")
            self.query.create_base()

        if self.delete_tables:
            print("DELETE table")
            self.query.clean_all_table()

        if self.recover_api_product:
            # Recover category
            categories = self.list_categories.split(", ")
            # Recover product by category
            for category in categories:
                api_category = RecoverApi().get_product(category)
                # We loop all product for collect info
                for product in api_category['products']:
                    name = product.get("product_name_fr", None)
                    stores = product.get("stores", None)
                    url = product.get("url", None)
                    nutriscore = product.get("nutriscore_score", None)
                    categories_product = product.get("categories", "")
                    # Category associated with the product
                    list_categories = categories_product.split(",")

                    self.query.add_product(
                        name_product=name,
                        stores=stores,
                        url=url,
                        nutriscore=nutriscore,
                        list_category=list_categories,
                    )


if __name__ == "__main__":
    Manage()
