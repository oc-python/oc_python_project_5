from operator import attrgetter

from sqlalchemy import create_engine, delete
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import exists
from sqlalchemy.ext.declarative import declarative_base

from tables.config.config import config
from tables.models_table import Product, Category, History, product_category, Base


class Query:
    """
    Class that does sql requests
    """
    def __init__(self):
        """
        init Query
        """
        self.engine = create_engine(config["SERVER"]["DB_MYSQL"])
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def add_product(self, name_product: str, stores: str, url: str, nutriscore: int, list_category: list):
        """
        Add product in DB
        :param name_product: str: Product name
        :param stores: str: list of stores where the product is sold
        :param url: str! url OpenFoodFact
        :param nutriscore: Number: Nutriscore of the product
        :param list_category: Product categories

        """
        product_present = self.session.query(exists().where(Product.url == url))

        if not product_present.scalar():
            print(name_product)
            product = Product(
                name=name_product, stores=stores, url=url, nutriscore=nutriscore
            )
            try:
                self.session.add(product)
                self.session.commit()
            except:
                print(
                    "Une erreur c'est produite lors de l'insertion du produit:"
                    f" {name_product}"
                )

            for category in list_category:
                category = category.strip()

                if category[2] == ":":
                    category = category[3:]

                select_cat = self.session.query(Category).filter_by(name=category)
                result_select_category = select_cat.all()

                if result_select_category:
                    product.categories.append(result_select_category[0])
                    self.session.commit()
                else:
                    categories = Category(name=category)
                    product.categories.append(categories)

                    self.session.add(categories)
                    self.session.commit()

    def select_category(self):
        """
        select category in the category table
        :return: dict with category
        """
        result = self.session.query(Category)
        list_categories = {}
        for name in result:
            list_categories[name.name] = name.id

        return list_categories

    def select_product_by_category(self, category: list):
        """
        Select products from the "category" category
        :param category: Category name
        :return: sql_alchemy object list
        """

        # Recover product id
        result_product = (
            self.session.query(Product)
            .join(product_category)
            .filter(product_category.columns.id_category == category[1])
            .all()
        )

        return result_product

    def get_substitut(self, product):
        """
        search the substitut
        :param product: product id to replace
        :return: substitut product list
        """
        sq = (
            self.session.query(product_category.columns.id_category)
            .filter(product_category.columns.id_product == product.id)
            .all()
        )

        list_cat = []
        list_id_sub = []
        # select of id products with id_cat
        for id_cat in sq:
            list_cat.append(id_cat.id_category)
            query = (
                self.session.query(product_category.columns.id_product)
                .filter(product_category.columns.id_category == id_cat.id_category)
                .all()
            )

            self.session.query(product_category)

            for id_prod in query:
                list_id_sub.append(id_prod.id_product)

        # delete duplicate
        list_id_sub = list(set(list_id_sub))
        del list_id_sub[list_id_sub.index(product.id)]

        # Query product substitut
        list_product_sub = []
        for id_sub in list_id_sub:

            if product.nutriscore is None:
                product_sub = (self.session.query(Product).filter(Product.id == id_sub).first())
            else:
                product_sub = (self.session.query(Product)
                               .filter(Product.id == id_sub, Product.nutriscore < product.nutriscore)
                               .first()
                               )

            if product_sub:
                list_product_sub.append(product_sub)

        list_product_sub_sorted = sorted(list_product_sub, key=attrgetter("nutriscore"))

        return list_product_sub_sorted

    def insert_history(self, product, product_sub):
        """
        Insert in history table
        :param product: str: Name product
        :param product_sub: str: Name substitut
        """
        history = History(id_user=1, id_aliment=product.id, id_substitut=product_sub.id)
        self.session.add(history)
        self.session.commit()

    def create_base(self):
        """
        Create all table in DB
        """
        Base.metadata.create_all(self.engine)

    def drop_all_table(self):
        """
        Delete all table in DB
        """
        Base.metadata.drop_all(self.engine)
        print("DROP SUCCESS")

    def clean_all_table(self):
        """
        Delete all data in the table
        """
        print("CLEAN TABLES")
        delete_table_product = delete(Product)
        delete_table_product_category = delete(product_category)
        delete_table_category = delete(Category)

        connection = self.engine.connect()
        connection.execute(delete_table_product)
        connection.execute(delete_table_product_category)
        connection.execute(delete_table_category)

    def select_product_substitut(self):
        """
        get substitut in DB
        """
        get_sub = self.session.query(History).all()

        list_history = []
        for result in get_sub:
            product_initial = (
                self.session.query(Product)
                .filter(Product.id == result.id_aliment)
                .all()
            )
            product_sub = (
                self.session.query(Product)
                .filter(Product.id == result.id_substitut)
                .all()
            )

            list_sub_product = [product_initial, product_sub]
            list_history.append(list_sub_product)

        return list_history
