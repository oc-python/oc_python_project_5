import requests

from .config.config import config


class RecoverApi:
    """
    request for get apî data
    """

    def get_product(self, category):
        """
        get api data for category list
        :param category: list: category
        :return: json: product list
        """
        params = {
            "action": "process",
            "json": 1,
            "tagtype_0": "categories",
            "tag_contains_0": "contains",
            "tag_0": category,
            "page_size": config['API']['limit']
        }

        print(f"Inserting {category} products into database")
        try:
            query = requests.get(config["API"]["url"], params=params)
            query.raise_for_status() #detect error
            query = query.json()
            return query
        except:
            print("Une erreur c'est produite pendant la requete GET")


if __name__ == "__main__":
    RecoverApi()
