import time

from tables.manage import Manage
from tables.query import Query
from tables.config.config import config


class Substitut:
    """
    Main class script
    it allows ask the question at the user
    """

    def __init__(self):
        """
        Constructor for Substitut
        """

        self.manage = Manage()
        self.query = Query()
        self.max_cat_display = 0

        if config["MANAGE"].getboolean("launch_program"):
            print("Bienvenue sur l'application Substitut")
            self.max_cat_display = int(config["CATEGORY"]["max_category_display"])
            self.get_product()

    def history(self):
        """
        load history before lauching script
        """
        print("Voulez vous récuperer l'historique de vos recherches ? (y or n)")

        while True:
            choose_input = input()

            if choose_input == "y":
                get_sub_list = self.query.select_product_substitut()
                index = 1
                for sub in get_sub_list:
                    print(
                        f"{index}: Produit initial: {sub[0][0].name} (nutriscore:"
                        f" {sub[0][0].nutriscore}) => Produit de subsitution:"
                        f" {sub[1][0].name} (nutriscore: {sub[1][0].nutriscore})"
                    )
                    index += 1
                print("Appuie sur 'n' pour continuer")
                while True:
                    choose_next = input()

                    if choose_next == "n":
                        return

                    print('Votre réponse est invalide, veuillez répondre par "n"')

            if choose_input == "n":
                break

            print('Votre réponse est invalide, veuillez répondre par "y" ou "n"')

    def category(self):
        """
        load category during question/script
        :return: chooses category
        """
        dict_categories = self.query.select_category()
        print(
            "Veuillez choisir une catégories parmis la listes suivante (tapez le numero"
            " correspondant):"
        )

        # Filter_loop is where for limit the display data
        filter_loop = self.max_cat_display - 1

        list_categories = []
        for index, item in enumerate(dict_categories.items()):

            list_categories.append(item)
            print(f"{index + 1}: {item[0]}")

            if index == filter_loop:
                break

        print("Faite votre choix:")

        while True:
            try:
                choose = int(input())
                if choose <= len(list_categories):
                    break

                print(
                    "Votre choix est invalide, veuillez choisir un nombre entre 1"
                    f" et {len(list_categories)}"
                )
            except ValueError:
                print("Veuillez entrez un nombre")

        choose_categorie = list_categories[(choose - 1)]
        print(
            f"Vous avez choisis le n°{choose}, ce qui correspond à la catégorie"
            f' "{choose_categorie[0]}"'
        )

        return choose_categorie

    def product(self, choose_categorie):
        """
        load product with ur categoryu
        :param choose_categorie: category choose
        :return: product chooses
        """
        print(
            "Veulliez choisir un produit appartenant à la catégorie"
            f' "{choose_categorie[0]}" (tapez le numero correspondant):'
        )

        select_product_by_id = self.query.select_product_by_category(choose_categorie)

        for index, product in enumerate(select_product_by_id):
            print(f"{index + 1}: {product.name}")

        while True:
            try:
                choose_product = int(input())

                if choose_product <= len(select_product_by_id):
                    break
                print(
                    "Votre choix est invalide, veuillez choisir un nombre entre 1"
                    f" et {len(select_product_by_id)}"
                )
            except ValueError:
                print("Veuillez entrez un nombre")
        select_product = select_product_by_id[int(choose_product) - 1]
        print(f"Vous avez choisi le produit {select_product.name}")
        print(
            f"Le produit {select_product.name} à un nutriscores de"
            f" {select_product.nutriscore} \n"
        )

        return select_product

    def substitute(self, select_product):
        """
        load substitute at ur product
        :param select_product: product choose by user
        :return: subsitute product
        """
        print("Voici une liste des produits de substitution:")
        print(
            "Le nutriscore est calculé par un système de points, le score le plus"
            " faible étant le meilleur "
        )

        select_sub = self.query.get_substitut(select_product)
        for index, product_sub in enumerate(select_sub):
            print(
                f"{index + 1}: {product_sub.name} pour un nutriscore de :"
                f" {product_sub.nutriscore}"
            )

        print("Tapez votre choix")

        while True:
            choose_product = int(input())

            if choose_product <= len(select_sub):
                break
            print(
                "Votre choix est invalide, veuillez choisir un nombre entre 1 et"
                f" {len(select_sub)}"
            )

        print(f'Vous avez choisis "{select_sub[choose_product - 1].name}"')

        print("Voulez vous plus d'information sur le produit ?? (y or n)")

        while True:
            choose_more_info = input()

            if choose_more_info == "y":
                print(f"Nom: {select_sub[choose_product - 1].name}")
                print(f"Magasin ou l'acheter: {select_sub[choose_product - 1].stores}")
                print(f"URL du produit: {select_sub[choose_product - 1].url}")
                break
            if choose_more_info == "n":
                break
            print(f'Votre réponse est invalide, veuillez répondre par "y" ou "n"')

        return select_sub, choose_product

    def register(self, select_product, select_sub):
        """
        register product in DB
        :param select_product: product choose by user
        :param select_sub: substitute choose for user
        :param choose_product: id product : int
        """
        print("Voulez vous l'enregistrer ? (y or n)")

        while True:
            choose_input = input()

            if choose_input == "y":
                try:
                    self.query.insert_history(
                        select_product, select_sub
                    )
                    print(
                        f"Le produit {select_sub.name} à était"
                        " enregistré avec succes"
                    )

                    break
                except:
                    print(
                        "Une erreur c'est produite lors de l'enregistrement du produit"
                    )
            elif choose_input == "n":
                break

            else:
                print('Votre réponse est invalide, veuillez répondre par "y" ou "n"')

    def get_product(self):
        """
        Question at the user
        """
        self.history()
        choose_categorie = self.category()
        time.sleep(1)
        select_product = self.product(choose_categorie)
        select_sub, choose_product = self.substitute(select_product)
        # self.register(select_product, select_sub, choose_product)
        self.register(select_product, select_sub[choose_product - 1])


        print("Bonne journée")


if __name__ == "__main__":
    Substitut()
