# Project_5: Utilisez les données publiques de l'OpenFoodFacts

[![Generic badge](https://img.shields.io/badge/OpenClassrooms-Project5-<>.svg)]()
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)]()
[![version-python](https://img.shields.io/static/v1?label=Python&message=3.7&color=065535)]()
[![version-mysqq](https://img.shields.io/static/v1?label=MySQL&message=14.14&color=065535)]()

# Détail du projet
###### Enoncé:
La startup Pur Beurre travaille connait bien les habitudes alimentaires françaises. 
Leur restaurant, Ratatouille, remporte un succès croissant et attire toujours plus de visiteurs sur la butte de Montmartre.

L'équipe a remarqué que leurs utilisateurs voulaient bien changer leur alimentation mais ne savaient pas bien par quoi commencer. 
Remplacer le Nutella par une pâte aux noisettes, oui, mais laquelle ? Et dans quel magasin l'acheter ? 
Leur idée est donc de créer un programme qui interagirait avec la base Open Food Facts pour en récupérer les aliments, les comparer et proposer à l'utilisateur un substitut plus sain à l'aliment qui lui fait envie.

###### Fonctionnalités:
- Rechercher un aliment dans la base de données [Open Food Fact](https://documenter.getpostman.com/view/8470508/SVtN3Wzy?version=latest)
- En interagissant avec le terminal, l'utilisateur choisis un catégorie puis le produit souhaité
- Si l'utilisateur entre un caractère qui n'est pas un chiffre, le programme doit lui répéter la question,
- La recherche doit s'effectuer sur une base MySql.


# Installation

###### Prérequis avant le lancement du programme

Retrouvez toutes les installations nécessaires dans [le wiki](https://gitlab.com/oc-python/oc_python_project_5/-/wikis/home)
```
https://gitlab.com/oc-python/oc_python_project_5/-/wikis/home
```

  
###### Lancement du programme

- Configurer le fichier config.ini:
 Au lancement du programme, et une fois votre base créé, il faut remplir le fichier ```config.ini```
 
```Create_table``` => Création des tables

```Drop_all_table``` => Suppression des tables

```launch_program``` => Lancement du programme 

```Insert_product``` => Insertion des produits via l'API

```delete_data_tables``` => Suppression des données dans les tables

```max_product_by_category``` => Limit de catégory lors du lancement du programme (evite d'avoir trop d'information à l'écran)

```list_categories``` => Mettre ici la category de produit que vous souhaitez inserer en DB


# Création des tables (via le script):

```
Product

id = Column(Integer, primary_key=True)
name = Column(String(255))
stores = Column(String(255))
url = Column(String(255))
nutriscore = Column(Integer)
``` 

```
category

id = Column(Integer, primary_key=True)
name = Column(String(255))
``` 

```
history

id = Column(Integer, primary_key=True)
id_user = Column(Integer)  # A mettre égal à 1
date_create = Column(DateTime, default=datetime.datetime.utcnow)
id_aliment = Column(Integer, ForeignKey("product.id"))
id_substitut = Column(Integer, ForeignKey("product.id"))
``` 

```
product_category

Column("id_product", Integer, ForeignKey("product.id")),
Column("id_category", Integer, ForeignKey("category.id")),
``` 


La création des tables se fait automatiquement (une fois que ```Create_table``` est sur ```True```) via sqlalchemy
Pour modifier les tables, vous pouvez les modifier directement dans le fichier tables/models_table.py

Les tables Product, Category et History sont créer directement à partir de lors class respective dans le fichier models_table.py
La table product_category est une table d'association créer en debut de ficher

# Lancement du programme:
Pour lancer le programme, il vous suffit de mettre ```launch_program``` sur ```True```.
Pensez également à passer tous les autres parametre sur ```False```


# Author
- [Eddy HUBERT](https://eddy-hubert.fr) - Développeur Python en Alternance
